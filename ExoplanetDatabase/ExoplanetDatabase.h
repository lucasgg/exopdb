//
//  ExoplanetDatabase.h
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Exoplanet;

@interface ExoplanetDatabase : NSObject

@property (readonly) NSDictionary *exoplanetsDictionary;
@property (readonly) NSArray *exoplanetsDictonaryIndex;

+ (ExoplanetDatabase *)sharedInstance;

@end
