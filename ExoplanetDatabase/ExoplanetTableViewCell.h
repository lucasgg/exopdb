//
//  ExoplanetTableViewCell.h
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExoplanetTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblExoplanetName;
@property (weak, nonatomic) IBOutlet UILabel *lblExoplanetDiscoveryYear;
@property (weak, nonatomic) IBOutlet UILabel *lblExoplanetLastUpdate;

@end
