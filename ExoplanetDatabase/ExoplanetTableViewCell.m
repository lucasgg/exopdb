//
//  ExoplanetTableViewCell.m
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "ExoplanetTableViewCell.h"

@implementation ExoplanetTableViewCell

- (void)awakeFromNib {
	// Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	
	// Configure the view for the selected state
}

@end
