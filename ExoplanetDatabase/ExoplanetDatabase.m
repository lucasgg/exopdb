//
//  ExoplanetDatabase.m
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "ExoplanetDatabase.h"
#import "Exoplanet.h"

@implementation ExoplanetDatabase

+ (instancetype)sharedInstance {
	// Create and/or return the shared singleton.
	static ExoplanetDatabase *sharedInstance;
	
	static dispatch_once_t once;
	
	dispatch_once(&once, ^{
		sharedInstance = [[ExoplanetDatabase alloc] init];
	});
	
	return sharedInstance;
}

- (instancetype)init
{
	self = [super init];
	if (self) {
		// The populateExoplanetsDictionary method already create the object.
		_exoplanetsDictionary = [self populateExoplanetsDictionary];
		
		if (_exoplanetsDictionary != nil)
			_exoplanetsDictonaryIndex = [[_exoplanetsDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
		else
			return nil;
	}
	return self;
}

- (NSDictionary *)populateExoplanetsDictionary {
	// Read PlanetDatabase.txt and populate the dictionary and the index array.
	NSBundle *bundle = [NSBundle mainBundle];
	NSError *erro;
	// Get the path for the database.
	NSString *path = [bundle pathForResource:@"PlanetDatabase" ofType:@"txt"];
	// Get the contents of the database.
	NSString *databaseContent = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&erro];
	// Get the lines of the archive and put all then in an array.
	NSArray *databaseContentLines = [databaseContent componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
	
	// Create an auxiliar mutable dictionary.
	NSMutableDictionary *exoplanetsAuxDictionary = [[NSMutableDictionary alloc] init];
	
	for (NSUInteger i = 0; i < [databaseContentLines count]; i++) {
		// Get line.
		NSString *databaseLine = [databaseContentLines objectAtIndex:i];
		
		// Verify if line is nil or empty or is a comment.
		if (databaseLine == nil || [databaseLine length] < EXOPLANET_MINIMAL_NAME_LENGTH || [databaseLine characterAtIndex:0] == '#') {
			// NSLog(@"%@", databaseLine);
			continue;
		}
		
		// NSLog(@"%@", databaseLine);
		
		// The components are separeted by commas, so get them.
		NSArray *databaseLineComponents = [databaseLine componentsSeparatedByString:@","];
		
		// Components count is (EXOPLANET_STAR_AGE + 1) because the enum index begins in 0.
		const NSUInteger databaseLineComponentsCount = (EXOPLANET_STAR_AGE + 1);
		
		// Verify if number of components match.
		if ([databaseLineComponents count] != databaseLineComponentsCount) {
			NSLog(@"Problem on the input %lu, number of components dosnt match %lu.", i + 1, (unsigned long)databaseLineComponentsCount);
			continue;
		}
		
		// Get the data.
		NSString *exoplanetIdentification = [databaseLineComponents objectAtIndex:EXOPLANET_IDENTIFICATION];
		NSUInteger exoplanetBinaryFlag = [[databaseLineComponents objectAtIndex:EXOPLANET_BINARY_FLAG] integerValue];
		NSNumber *exoplanetMass = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_MASS] doubleValue]];
		NSNumber *exoplanetRadius = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_RADIUS] doubleValue]];
		NSNumber *exoplanetPeriod = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_PERIOD] doubleValue]];
		NSNumber *exoplanetSemiMajorAxis = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_SEMI_MAJOR_AXIS] doubleValue]];
		
		// Verify eccentricity to dont consider empty value as 0.0f value, considering as -1.0.
		
		NSString *exoplanetEccentricityStr = [databaseLineComponents objectAtIndex:EXOPLANET_ECCENTRICITY];
		NSNumber *exoplanetEccentricity;
		if (exoplanetEccentricityStr == nil || [exoplanetEccentricityStr length] == 0)
			exoplanetEccentricity = [NSNumber numberWithDouble:(-1.0f)];
		else
			exoplanetEccentricity = [NSNumber numberWithDouble:[exoplanetEccentricityStr doubleValue]];
		
		// Verify the four degree values to dont consider empty value as 0.0 degree, considering as 1000.0f.

		NSString *exoplanetPeriastronStr = [databaseLineComponents objectAtIndex:EXOPLANET_PERIASTRON];
		NSNumber *exoplanetPeriastron;
		if (exoplanetPeriastronStr == nil || [exoplanetPeriastronStr length] == 0)
			exoplanetPeriastron = [NSNumber numberWithDouble:(1000.0f)];
		else
			exoplanetPeriastron = [NSNumber numberWithDouble:[exoplanetPeriastronStr doubleValue]];
		
		NSString *exoplanetLongitudeStr = [databaseLineComponents objectAtIndex:EXOPLANET_LONGITUDE];
		NSNumber *exoplanetLongitude;
		if (exoplanetLongitudeStr == nil || [exoplanetLongitudeStr length] == 0)
			exoplanetLongitude = [NSNumber numberWithDouble:(1000.0f)];
		else
			exoplanetLongitude = [NSNumber numberWithDouble:[exoplanetLongitudeStr doubleValue]];
		
		NSString *exoplanetAscendingNodeStr = [databaseLineComponents objectAtIndex:EXOPLANET_ASCENDING_NODE];
		NSNumber *exoplanetAscendingNode;
		if (exoplanetAscendingNodeStr == nil || [exoplanetAscendingNodeStr length] == 0)
			exoplanetAscendingNode = [NSNumber numberWithDouble:(1000.0f)];
		else
			exoplanetAscendingNode = [NSNumber numberWithDouble:[exoplanetAscendingNodeStr doubleValue]];
		
		NSString *exoplanetInclinationStr = [databaseLineComponents objectAtIndex:EXOPLANET_INCLINATION];
		NSNumber *exoplanetInclination;
		if (exoplanetInclinationStr == nil || [exoplanetInclinationStr length] == 0)
			exoplanetInclination = [NSNumber numberWithDouble:(1000.0f)];
		else
			exoplanetInclination = [NSNumber numberWithDouble:[exoplanetInclinationStr doubleValue]];
		
		// Verify equilibrium temperature to dont consider empty value as 0.0f value, considering as -400.0f.
		
		NSString *exoplanetEquilibriumTemperatureStr = [databaseLineComponents objectAtIndex:EXOPLANET_EQUILIBRIUM_TEMPERATURE];
		NSNumber *exoplanetEquilibriumTemperature;
		if (exoplanetEquilibriumTemperatureStr == nil || [exoplanetEquilibriumTemperatureStr length] == 0)
			exoplanetEquilibriumTemperature = [NSNumber numberWithDouble:(-400.0f)];
		else
			exoplanetEquilibriumTemperature = [NSNumber numberWithDouble:[exoplanetEquilibriumTemperatureStr doubleValue]];
		
		NSNumber *exoplanetAge = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_AGE] doubleValue]];
		NSString *exoplanetDiscoveryMethod = [databaseLineComponents objectAtIndex:EXOPLANET_DISCOVERY_METHOD];
		NSUInteger exoplanetDiscoveryYear = [[databaseLineComponents objectAtIndex:EXOPLANET_DISCOVERY_YEAR] integerValue];
		NSString *exoplanetLastUpdate = [databaseLineComponents objectAtIndex:EXOPLANET_LAST_UPDATE];
		NSString *exoplanetRightAscesion = [databaseLineComponents objectAtIndex:EXOPLANET_RIGHT_ASCESION];
		NSString *exoplanetDeclination = [databaseLineComponents objectAtIndex:EXOPLANET_DECLINATION];
		NSNumber *exoplanetDistanceFromSun = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_DISTANCE_FROM_SUN] doubleValue]];
		NSNumber *exoplanetStarMass = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_STAR_MASS] doubleValue]];
		NSNumber *exoplanetStarRadius = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_STAR_RADIUS] doubleValue]];
		NSNumber *exoplanetStarMetalicity = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_STAR_METALICITY] doubleValue]];
		NSNumber *exoplanetStarTemperature = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_STAR_TEMPERATURE] doubleValue]];
		NSNumber *exoplanetStarAge = [NSNumber numberWithDouble:[[databaseLineComponents objectAtIndex:EXOPLANET_STAR_AGE] doubleValue]];
		
		/* // For debugging only.
		 if (exoplanetDiscoveryMethod != nil && [exoplanetDiscoveryMethod length] > 0 && ![exoplanetDiscoveryMethod isEqualToString:@"transit"] && ![exoplanetDiscoveryMethod isEqualToString:@"microlensing"] && ![exoplanetDiscoveryMethod isEqualToString:@"RV"] && ![exoplanetDiscoveryMethod isEqualToString:@"timing"] && ![exoplanetDiscoveryMethod isEqualToString:@"imaging"]) {
			NSLog(@"%@", exoplanetDiscoveryMethod);
		 }
		 */
		
		// Get the key for the dictionary.
		NSString *exoplanetsDictionaryKey = [NSString stringWithFormat:@"%c", [exoplanetIdentification characterAtIndex:0]];
		
		// NSLog(@"%@", exoplanetsDictionaryKey);
		
		// Create the exoplanet.
		Exoplanet *exoplanet = [Exoplanet exoplanetWithId:exoplanetIdentification andBinaryFlag:exoplanetBinaryFlag andMass:exoplanetMass andRadius:exoplanetRadius andPeriod:exoplanetPeriod andSemiMajorAxis:exoplanetSemiMajorAxis andEccentricity:exoplanetEccentricity andPeriastron:exoplanetPeriastron andLongitude:exoplanetLongitude andAscendingNode:exoplanetAscendingNode andInclination:exoplanetInclination andEquilibriumTemperature:exoplanetEquilibriumTemperature andAge:exoplanetAge andDiscoveryMethod:exoplanetDiscoveryMethod andDiscoveryYear:exoplanetDiscoveryYear andLastUpdate:exoplanetLastUpdate andRightAscesion:exoplanetRightAscesion andDeclination:exoplanetDeclination andDistanceFromSun:exoplanetDistanceFromSun andStarMass:exoplanetStarMass andStarRadius:exoplanetStarRadius andStarMetalicity:exoplanetStarMetalicity andStarTemperature:exoplanetStarTemperature andStarAge:exoplanetStarAge];
		
		// Add exoplanet to dictionary if different from nil.
		if (exoplanet != nil) {
			NSMutableArray *exoplanetsDictionaryKeyArray = [NSMutableArray arrayWithArray:[exoplanetsAuxDictionary objectForKey:exoplanetsDictionaryKey]];
			[exoplanetsDictionaryKeyArray addObject:exoplanet];
			[exoplanetsAuxDictionary setObject:[NSArray arrayWithArray:exoplanetsDictionaryKeyArray] forKey:exoplanetsDictionaryKey];
		}
	}
	
	if ([exoplanetsAuxDictionary count] != 0)
		return [NSDictionary dictionaryWithDictionary:exoplanetsAuxDictionary];
	else
		return nil;
}

@end
