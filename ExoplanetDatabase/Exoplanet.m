//
//  Exoplanet.m
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "Exoplanet.h"

@implementation Exoplanet

+ (instancetype)exoplanetWithId:(NSString *)identification andBinaryFlag:(NSUInteger)binaryFlag andMass:(NSNumber *)mass andRadius:(NSNumber *)radius andPeriod:(NSNumber *)period andSemiMajorAxis:(NSNumber *)semiMajorAxis andEccentricity:(NSNumber *)eccentricity andPeriastron:(NSNumber *)periastron andLongitude:(NSNumber *)longitude andAscendingNode:(NSNumber *)ascendingNode andInclination:(NSNumber *)inclination andEquilibriumTemperature:(NSNumber *)equilibriumTemperature andAge:(NSNumber *)age andDiscoveryMethod:(NSString *)discoveryMethod andDiscoveryYear:(NSUInteger)discoveryYear andLastUpdate:(NSString *)lastUpdate andRightAscesion:(NSString *)rightAscesion andDeclination:(NSString *)declination andDistanceFromSun:(NSNumber *)distanceFromSun andStarMass:(NSNumber *)starMass andStarRadius:(NSNumber *)starRadius andStarMetalicity:(NSNumber *)starMetalicity andStarTemperature:(NSNumber *)starTemperature andStarAge:(NSNumber *)starAge {
	return [[Exoplanet alloc] initWithId:identification andBinaryFlag:binaryFlag andMass:mass andRadius:radius andPeriod:period andSemiMajorAxis:semiMajorAxis andEccentricity:eccentricity andPeriastron:periastron andLongitude:longitude andAscendingNode:ascendingNode andInclination:inclination andEquilibriumTemperature:equilibriumTemperature andAge:age andDiscoveryMethod:discoveryMethod andDiscoveryYear:discoveryYear andLastUpdate:lastUpdate andRightAscesion:rightAscesion andDeclination:declination andDistanceFromSun:distanceFromSun andStarMass:starMass andStarRadius:starRadius andStarMetalicity:starMetalicity andStarTemperature:starTemperature andStarAge:starAge];
}

- (instancetype)initWithId:(NSString *)identification andBinaryFlag:(NSUInteger)binaryFlag andMass:(NSNumber *)mass andRadius:(NSNumber *)radius andPeriod:(NSNumber *)period andSemiMajorAxis:(NSNumber *)semiMajorAxis andEccentricity:(NSNumber *)eccentricity andPeriastron:(NSNumber *)periastron andLongitude:(NSNumber *)longitude andAscendingNode:(NSNumber *)ascendingNode andInclination:(NSNumber *)inclination andEquilibriumTemperature:(NSNumber *)equilibriumTemperature andAge:(NSNumber *)age andDiscoveryMethod:(NSString *)discoveryMethod andDiscoveryYear:(NSUInteger)discoveryYear andLastUpdate:(NSString *)lastUpdate andRightAscesion:(NSString *)rightAscesion andDeclination:(NSString *)declination andDistanceFromSun:(NSNumber *)distanceFromSun andStarMass:(NSNumber *)starMass andStarRadius:(NSNumber *)starRadius andStarMetalicity:(NSNumber *)starMetalicity andStarTemperature:(NSNumber *)starTemperature andStarAge:(NSNumber *)starAge {
	self = [super init];
	if (self) {
		if (identification == nil || [identification length] < EXOPLANET_MINIMAL_NAME_LENGTH) {
			NSLog(@"Exoplanet class, initWithId: Exoplanet identification must not be nil, empty or with a lenght less than the minimal.");
			return nil;
		}
		
		// Planet information.
		_identification = identification;
		_binaryFlag = binaryFlag;
		_mass = mass;
		_radius = radius;
		_period = period;
		_semiMajorAxis = semiMajorAxis;
		_eccentricity = eccentricity;
		_periastron = periastron;
		_longitude = longitude;
		_ascendingNode = ascendingNode;
		_inclination = inclination;
		_equilibriumTemperature = equilibriumTemperature;
		_age = age;
		_discoveryMethod = discoveryMethod;
		_discoveryYear = discoveryYear;
		_lastUpdate = lastUpdate;
		_rightAscesion = rightAscesion;
		_declination = declination;
		_distanceFromSun = distanceFromSun;
		// Star information.
		_starMass = starMass;
		_starRadius = starRadius;
		_starMetalicity = starMetalicity;
		_starTemperature = starTemperature;
		_starAge = starAge;
	}
	return self;
}

@end
