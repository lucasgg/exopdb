//
//  Exoplanet.h
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <Foundation/Foundation.h>

// This is the minimal name size for planet identification.
#define EXOPLANET_MINIMAL_NAME_LENGTH 2

// Jupiter mass in kg.
#define EXOPLANET_JUPITER_MASS		1.8986e27f
// Jupiter radius in km.
#define EXOPLANET_JUPITER_RADIUS	69911.f
// Astronomical unit in km.
#define EXOPLANET_ASTRONOMICAL_UNIT	1.496e8f
// Gyr in billions.
#define EXOPLANET_GYR			1000000000.f
// Solar mass in kg.
#define EXOPLANET_SOLAR_MASS		1.98855e30f
// Solar radius in km.
#define EXOPLANET_SOLAR_RADIUS		696342.f

// This enum can be used to get the data from database file and populate this exoplanet class.
typedef NS_ENUM(NSInteger, EXOPLANET) {
	EXOPLANET_IDENTIFICATION,
	EXOPLANET_BINARY_FLAG,
	EXOPLANET_MASS,
	EXOPLANET_RADIUS,
	EXOPLANET_PERIOD,
	EXOPLANET_SEMI_MAJOR_AXIS,
	EXOPLANET_ECCENTRICITY,
	EXOPLANET_PERIASTRON,
	EXOPLANET_LONGITUDE,
	EXOPLANET_ASCENDING_NODE,
	EXOPLANET_INCLINATION,
	EXOPLANET_EQUILIBRIUM_TEMPERATURE,
	EXOPLANET_AGE,
	EXOPLANET_DISCOVERY_METHOD,
	EXOPLANET_DISCOVERY_YEAR,
	EXOPLANET_LAST_UPDATE,
	EXOPLANET_RIGHT_ASCESION,
	EXOPLANET_DECLINATION,
	EXOPLANET_DISTANCE_FROM_SUN,
	EXOPLANET_STAR_MASS,
	EXOPLANET_STAR_RADIUS,
	EXOPLANET_STAR_METALICITY,
	EXOPLANET_STAR_TEMPERATURE,
	EXOPLANET_STAR_AGE
};

@interface Exoplanet : NSObject

/* Planet */
@property (readonly) NSString *identification; // Identifier of the planet.
@property (readonly) NSUInteger binaryFlag; // Binary flag.
@property (readonly) NSNumber *mass; // Mass in Jupiter units.
@property (readonly) NSNumber *radius; // Radius in Jupiter units.
@property (readonly) NSNumber *period; // Period of own rotation in days.
@property (readonly) NSNumber *semiMajorAxis; // Semi-major axis in AU.
@property (readonly) NSNumber *eccentricity; // Eccentricity of the planet (no unit).
@property (readonly) NSNumber *periastron; // Periastron (degree).
@property (readonly) NSNumber *longitude; // Longitude (degree).
@property (readonly) NSNumber *ascendingNode; // Longitude of ascending node (degree).
@property (readonly) NSNumber *inclination; // Inclination (degree).
@property (readonly) NSNumber *equilibriumTemperature; // Equilibrium temperature in Kelvins.
@property (readonly) NSNumber *age; // Age of the planet in Gyr units.
@property (readonly) NSString *discoveryMethod; // Discovery method used to discover the planet.
@property (readonly) NSUInteger discoveryYear; // Year of the discovery in yyyy.
@property (readonly) NSString *lastUpdate; // Last update of the planet information in yy/mm/dd.
@property (readonly) NSString *rightAscesion; // Right ascesion in hh mm ss.
@property (readonly) NSString *declination; // Declination in (+-angle mm ss).
@property (readonly) NSNumber *distanceFromSun; // Distance from (our) Sun in parsec.

/* Star */
@property (readonly) NSNumber *starMass; // Mass in Solar units.
@property (readonly) NSNumber *starRadius; // Radius in Solar units.
@property (readonly) NSNumber *starMetalicity; // Metalicity in Solar units.
@property (readonly) NSNumber *starTemperature; // Temperature in Kelvins.
@property (readonly) NSNumber *starAge; // Age in Gyr.

+ (instancetype)exoplanetWithId:(NSString *)identification andBinaryFlag:(NSUInteger)binaryFlag andMass:(NSNumber *)mass andRadius:(NSNumber *)radius andPeriod:(NSNumber *)period andSemiMajorAxis:(NSNumber *)semiMajorAxis andEccentricity:(NSNumber *)eccentricity andPeriastron:(NSNumber *)periastron andLongitude:(NSNumber *)longitude andAscendingNode:(NSNumber *)ascendingNode andInclination:(NSNumber *)inclination andEquilibriumTemperature:(NSNumber *)equilibriumTemperature andAge:(NSNumber *)age andDiscoveryMethod:(NSString *)discoveryMethod andDiscoveryYear:(NSUInteger)discoveryYear andLastUpdate:(NSString *)lastUpdate andRightAscesion:(NSString *)rightAscesion andDeclination:(NSString *)declination andDistanceFromSun:(NSNumber *)distanceFromSun andStarMass:(NSNumber *)starMass andStarRadius:(NSNumber *)starRadius andStarMetalicity:(NSNumber *)starMetalicity andStarTemperature:(NSNumber *)starTemperature andStarAge:(NSNumber *)starAge;

- (instancetype)initWithId:(NSString *)identification andBinaryFlag:(NSUInteger)binaryFlag andMass:(NSNumber *)mass andRadius:(NSNumber *)radius andPeriod:(NSNumber *)period andSemiMajorAxis:(NSNumber *)semiMajorAxis andEccentricity:(NSNumber *)eccentricity andPeriastron:(NSNumber *)periastron andLongitude:(NSNumber *)longitude andAscendingNode:(NSNumber *)ascendingNode andInclination:(NSNumber *)inclination andEquilibriumTemperature:(NSNumber *)equilibriumTemperature andAge:(NSNumber *)age andDiscoveryMethod:(NSString *)discoveryMethod andDiscoveryYear:(NSUInteger)discoveryYear andLastUpdate:(NSString *)lastUpdate andRightAscesion:(NSString *)rightAscesion andDeclination:(NSString *)declination andDistanceFromSun:(NSNumber *)distanceFromSun andStarMass:(NSNumber *)starMass andStarRadius:(NSNumber *)starRadius andStarMetalicity:(NSNumber *)starMetalicity andStarTemperature:(NSNumber *)starTemperature andStarAge:(NSNumber *)starAge;

@end
