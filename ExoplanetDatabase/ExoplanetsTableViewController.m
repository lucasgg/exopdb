//
//  ExoplanetsTableViewController.m
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "ExoplanetsTableViewController.h"
#import "ExoplanetTableViewCell.h"
#import "PlanetViewController.h"
#import "ExoplanetDatabase.h"
#import "Exoplanet.h"

@interface ExoplanetsTableViewController () {
	NSDictionary *mainDatabase;
	NSArray *mainDatabaseIndex;
}

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *gestureSearch;

@end

@implementation ExoplanetsTableViewController {
	NSString *searchTextInitialLetter;
	NSArray *searchResults;
	BOOL searching;
	
	// For performSegue to Planet View.
	Exoplanet *selectedExoplanet;
}

- (void)viewDidLoad {
	[super viewDidLoad];

	// Get the database and the index.
	mainDatabase = [[ExoplanetDatabase sharedInstance] exoplanetsDictionary];
	mainDatabaseIndex = [[ExoplanetDatabase sharedInstance] exoplanetsDictonaryIndex];
	
	// Set the searching to NO.
	searching = NO;
	
	//  Disable gesture.
	[_gestureSearch setEnabled:NO];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma tableview sections and cells

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	// Return the number of sections.
	if (searching && searchTextInitialLetter == nil)
		return 0;
	else if (searching)
		return 1;
	else
		return [mainDatabaseIndex count];
	
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	// Return the title for each header.
	if (searching)
		return searchTextInitialLetter;
	else
		return [mainDatabaseIndex objectAtIndex:section];
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
	// Return the array for index on the tableview.
	if (searching)
		return nil;
	else
		return mainDatabaseIndex;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	// Return the number of rows in the section.
	if (searching)
		return [searchResults count];
	else {
		NSString *sectionTitle = [mainDatabaseIndex objectAtIndex:section];
		NSArray *sectionExoplanets = [mainDatabase objectForKey:sectionTitle];
		return [sectionExoplanets count];
	}
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	ExoplanetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"planetCell" forIndexPath:indexPath];
	
	if (searching) {
		// Get the exoplanet at the search results.
		Exoplanet *exoplanet = [searchResults objectAtIndex:[indexPath row]];
		
		// Configure the cell labels.
		
		[[cell lblExoplanetName] setText:[exoplanet identification]];
		
		if ([exoplanet discoveryYear] == 0)
			[[cell lblExoplanetDiscoveryYear] setText:@"-"];
		else
			[[cell lblExoplanetDiscoveryYear] setText:[NSString stringWithFormat:@"%lu", (unsigned long)[exoplanet discoveryYear]]];
		
		// The lastUpdate is yy/mm/dd!
		[[cell lblExoplanetLastUpdate] setText:[exoplanet lastUpdate]];
	} else {
		// Get the information for the section and the cell.
		NSString *sectionTitle = [mainDatabaseIndex objectAtIndex:[indexPath section]];
		NSArray *sectionExoplanets = [mainDatabase objectForKey:sectionTitle];
		Exoplanet *exoplanet = [sectionExoplanets objectAtIndex:[indexPath row]];
		
		// Configure the cell labels.
		
		[[cell lblExoplanetName] setText:[exoplanet identification]];
		
		if ([exoplanet discoveryYear] == 0)
			[[cell lblExoplanetDiscoveryYear] setText:@"-"];
		else
			[[cell lblExoplanetDiscoveryYear] setText:[NSString stringWithFormat:@"%lu", (unsigned long)[exoplanet discoveryYear]]];
		
		// The lastUpdate is yy/mm/dd!
		[[cell lblExoplanetLastUpdate] setText:[exoplanet lastUpdate]];
	}
	
	// Return the cell.
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// Action to selected item.
	if (searching)
		selectedExoplanet = [searchResults objectAtIndex:[indexPath row]];
	else {
		// Get the information for the section and the cell.
		NSString *sectionTitle = [mainDatabaseIndex objectAtIndex:[indexPath section]];
		NSArray *sectionExoplanets = [mainDatabase objectForKey:sectionTitle];
		selectedExoplanet = [sectionExoplanets objectAtIndex:[indexPath row]];
	}
	
	// Perform the segue, calling prepareForSegue.
	[self performSegueWithIdentifier:@"toPlanetView" sender:self];
}

#pragma Tableview search

- (BOOL)filterContentForSearchText:(NSString*)searchText
{
	// Verify if searchText is nil or empty.
	if (searchText == nil || [searchText isEqualToString:@""])
		return NO;
	
	// This method will help me with the search.
	NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"identification contains[cd] %@", searchText];
	
	// Get the key.
	searchTextInitialLetter = [NSString stringWithFormat:@"%c", [searchText characterAtIndex:0]];
	
	// Get the array related to the key.
	NSArray *elementsToBeFiltered = [mainDatabase objectForKey:searchTextInitialLetter];
	
	// Verify if section exist.
	if ([elementsToBeFiltered count] > 0) {
		// Apply the search predicate and return YES for success.
		searchResults = [elementsToBeFiltered filteredArrayUsingPredicate:resultPredicate];
		
		// Set initial letter to nil to disable index if nothing has found.
		if ([searchResults count] == 0)
			searchTextInitialLetter = nil;
		
		return YES;
	} else {
		searchTextInitialLetter = nil;
		
		return YES;
	}
	
}

- (IBAction)dismissKeyboard:(id)sender {
	// Close the keyboard.
	[_searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	// Close keyboard.
	[searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	// Close keyboard.
	[searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
	// Show cancel button.
	[searchBar setShowsCancelButton:YES];
	
	// Enable gesture.
	[_gestureSearch setEnabled:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
	// Hidde cancel button.
	[searchBar setShowsCancelButton:NO];
	
	//  Disable gesture.
	[_gestureSearch setEnabled:NO];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	// Get search texts.
	if ([self filterContentForSearchText:[searchBar text]])
		searching = YES;
	else
		searching = NO;
	
	[[self tableView] reloadData];
}

#pragma Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	// Get the new view controller using [segue destinationViewController].
	// Pass the selected object to the new view controller.
	PlanetViewController *pvc = [segue destinationViewController];
	
	// Set the selected exoplanet. :)
	[pvc setExoplanet:selectedExoplanet];
}

@end
