//
//  ViewController.h
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Exoplanet;

@interface PlanetViewController : UIViewController

@property Exoplanet *exoplanet;

@end

