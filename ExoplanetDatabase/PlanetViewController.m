//
//  ViewController.m
//  ExoplanetDatabase
//
//  Created by Lucas Guimarães Gonçalves on 24/03/15.
//  Copyright (c) 2015 LGG. All rights reserved.
//

#import "PlanetViewController.h"
#import "Exoplanet.h"

@interface PlanetViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblPlanet;
@property (weak, nonatomic) IBOutlet UILabel *lblBinarySystem;
@property (weak, nonatomic) IBOutlet UILabel *lblMass;
@property (weak, nonatomic) IBOutlet UILabel *lblRadius;
@property (weak, nonatomic) IBOutlet UILabel *lblPeriod;
@property (weak, nonatomic) IBOutlet UILabel *lblSemimajorAxis;
@property (weak, nonatomic) IBOutlet UILabel *lblEccentricity;
@property (weak, nonatomic) IBOutlet UILabel *lblPeriastron;
@property (weak, nonatomic) IBOutlet UILabel *lblLongitude;
@property (weak, nonatomic) IBOutlet UILabel *lblAscendingNode;
@property (weak, nonatomic) IBOutlet UILabel *lblInclination;
@property (weak, nonatomic) IBOutlet UILabel *lblEquilibriumTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lblAge;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscoveryMethod;
@property (weak, nonatomic) IBOutlet UILabel *lblDiscoveryYear;
@property (weak, nonatomic) IBOutlet UILabel *lblLastUpdate;
@property (weak, nonatomic) IBOutlet UILabel *lblRightAscesion;
@property (weak, nonatomic) IBOutlet UILabel *lblDeclination;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceFromSun;
@property (weak, nonatomic) IBOutlet UILabel *lblStarMass;
@property (weak, nonatomic) IBOutlet UILabel *lblStarRadius;
@property (weak, nonatomic) IBOutlet UILabel *lblStarMetalicity;
@property (weak, nonatomic) IBOutlet UILabel *lblStarTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lblStarAge;

@end

@implementation PlanetViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	
	// Set the texts :D
	[_lblPlanet setText:[_exoplanet identification]];
	
	switch([_exoplanet binaryFlag]) {
		case 0:
			[_lblBinarySystem setText:@"Nonexistent or unknown."];
			break;
			
		case 1:
			[_lblBinarySystem setText:@"Type P."];
			break;
			
		case 2:
			[_lblBinarySystem setText:@"Type S."];
			break;
			
		case 3:
			[_lblBinarySystem setText:@"Orphan."];
			break;
			
		default:
			[_lblBinarySystem setText:@"-"];
			break;
	}
	
	if ([[_exoplanet mass] doubleValue] != 0.0f)
		[_lblMass setText:[NSString stringWithFormat:@"%.02f Jupiter mass units (%.0f kg)", [[_exoplanet mass] doubleValue], [[_exoplanet mass] doubleValue] * EXOPLANET_JUPITER_MASS]];
	else
		[_lblMass setText:@"-"];
	
	if ([[_exoplanet radius] doubleValue] != 0.0f)
		[_lblRadius setText:[NSString stringWithFormat:@"%.02f Jupiter radius units (%.0f km)", [[_exoplanet radius] doubleValue], [[_exoplanet radius] doubleValue] * EXOPLANET_JUPITER_RADIUS]];
	else
		[_lblRadius setText:@"-"];
	
	if ([[_exoplanet period] doubleValue] != 0.0f)
		[_lblPeriod setText:[NSString stringWithFormat:@"%.02f days", [[_exoplanet period] doubleValue]]];
	else
		[_lblPeriod setText:@"-"];
	
	if ([[_exoplanet semiMajorAxis] doubleValue] != 0.0f)
		[_lblSemimajorAxis setText:[NSString stringWithFormat:@"%.02f AU (%.0f km)", [[_exoplanet semiMajorAxis] doubleValue], [[_exoplanet semiMajorAxis] doubleValue] * EXOPLANET_ASTRONOMICAL_UNIT]];
	else
		[_lblSemimajorAxis setText:@"-"];
	
	if ([[_exoplanet eccentricity] doubleValue] < -0.005f)
		[_lblEccentricity setText:@"-"];
	else if ([[_exoplanet eccentricity] doubleValue] >= -0.005f && [[_exoplanet eccentricity] doubleValue] < 0.005f)
		[_lblEccentricity setText:[NSString stringWithFormat:@"Circular orbit (%f)", [[_exoplanet eccentricity] doubleValue]]];
	else if ([[_exoplanet eccentricity] doubleValue] >= 0.005f && [[_exoplanet eccentricity] doubleValue] < 0.995f)
		[_lblEccentricity setText:[NSString stringWithFormat:@"Elliptic orbit (%f)", [[_exoplanet eccentricity] doubleValue]]];
	else if ([[_exoplanet eccentricity] doubleValue] >= 0.995f && [[_exoplanet eccentricity] doubleValue] < 1.005f)
		[_lblEccentricity setText:[NSString stringWithFormat:@"Parabolic trajectory (%f)", [[_exoplanet eccentricity] doubleValue]]];
	else if ([[_exoplanet eccentricity] doubleValue] >= 1.005f)
		[_lblEccentricity setText:[NSString stringWithFormat:@"Hyperbolic trajectory (%f)", [[_exoplanet eccentricity] doubleValue]]];
	
	if ([[_exoplanet periastron] doubleValue] >= -360.0f && [[_exoplanet periastron] doubleValue] <= 360.0f)
		[_lblPeriastron setText:[NSString stringWithFormat:@"%.02f°", [[_exoplanet periastron] doubleValue]]];
	else
		[_lblPeriastron setText:@"-"];
	
	if ([[_exoplanet longitude] doubleValue] >= -360.0f && [[_exoplanet longitude] doubleValue] <= 360.0f)
		[_lblLongitude setText:[NSString stringWithFormat:@"%.02f°", [[_exoplanet longitude] doubleValue]]];
	else
		[_lblLongitude setText:@"-"];
	
	if ([[_exoplanet ascendingNode] doubleValue] >= -360.0f && [[_exoplanet ascendingNode] doubleValue] <= 360.0f)
		[_lblAscendingNode setText:[NSString stringWithFormat:@"%.02f°", [[_exoplanet longitude] doubleValue]]];
	else
		[_lblAscendingNode setText:@"-"];
	
	if ([[_exoplanet inclination] doubleValue] >= -360.0f && [[_exoplanet inclination] doubleValue] <= 360.0f)
		[_lblInclination setText:[NSString stringWithFormat:@"%.02f°", [[_exoplanet inclination] doubleValue]]];
	else
		[_lblInclination setText:@"-"];
	
	if ([[_exoplanet equilibriumTemperature] doubleValue] > -300.0f)
		[_lblEquilibriumTemperature setText:[NSString stringWithFormat:@"%.02f K", [[_exoplanet equilibriumTemperature] doubleValue]]];
	else
		[_lblEquilibriumTemperature setText:@"-"];
	
	if ([[_exoplanet age] doubleValue] != 0.0f)
		[_lblAge setText:[NSString stringWithFormat:@"%.02f Gyr (%f years)", [[_exoplanet age] doubleValue], [[_exoplanet age] doubleValue] * EXOPLANET_GYR]];
	else
		[_lblAge setText:@"-"];
	
	if ([_exoplanet discoveryMethod] == nil || [[_exoplanet discoveryMethod] isEqualToString:@""])
		[_lblDiscoveryMethod setText:@"-"];
	else
		[_lblDiscoveryMethod setText:[_exoplanet discoveryMethod]];
	
	if ([_exoplanet discoveryYear] != 0)
		[_lblDiscoveryYear setText:[NSString stringWithFormat:@"%lu", (unsigned long)[_exoplanet discoveryYear]]];
	else
		[_lblDiscoveryYear setText:@"-"];
	
	if ([_exoplanet lastUpdate] == nil || [[_exoplanet lastUpdate] isEqualToString:@""])
		[_lblLastUpdate setText:@"-"];
	else
		[_lblLastUpdate setText:[_exoplanet lastUpdate]];
	
	if ([_exoplanet rightAscesion] == nil || [[_exoplanet rightAscesion] isEqualToString:@""])
		[_lblRightAscesion setText:@"-"];
	else
		[_lblRightAscesion setText:[_exoplanet rightAscesion]];
	
	if ([_exoplanet declination] == nil || [[_exoplanet declination] isEqualToString:@""])
		[_lblDeclination setText:@"-"];
	else
		[_lblDeclination setText:[_exoplanet declination]];
	
	if ([[_exoplanet distanceFromSun] doubleValue] > 0.0f)
		[_lblDistanceFromSun setText:[NSString stringWithFormat:@"%.02f parsec", [[_exoplanet distanceFromSun] doubleValue]]];
	else
		[_lblDistanceFromSun setText:@"-"];
	
	// I dont want to use Sun mass or Sun other units here.
	
	if ([[_exoplanet starMass] doubleValue] > 0.0f)
		[_lblStarMass setText:[NSString stringWithFormat:@"%.02f Sun mass units", [[_exoplanet starMass] doubleValue]]];
	else
		[_lblStarMass setText:@"-"];
	
	if ([[_exoplanet starRadius] doubleValue] > 0.0f)
		[_lblStarRadius setText:[NSString stringWithFormat:@"%.02f Sun radius units", [[_exoplanet starRadius] doubleValue]]];
	else
		[_lblStarRadius setText:@"-"];
	
	if ([[_exoplanet starMetalicity] doubleValue] > 0.0f)
		[_lblStarMetalicity setText:[NSString stringWithFormat:@"%.02f", [[_exoplanet starMetalicity] doubleValue]]];
	else
		[_lblStarMetalicity setText:@"-"];
	
	if ([[_exoplanet starTemperature] doubleValue] != 0.0f)
		[_lblStarTemperature setText:[NSString stringWithFormat:@"%.02f K", [[_exoplanet starTemperature] doubleValue]]];
	else
		[_lblStarTemperature setText:@"-"];
	
	if ([[_exoplanet starAge] doubleValue] > 0.0f)
		[_lblStarAge setText:[NSString stringWithFormat:@"%.02f Gyr", [[_exoplanet starAge] doubleValue]]];
	else
		[_lblStarAge setText:@"-"];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
